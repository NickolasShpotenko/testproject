using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using UIKit;

namespace Test.IOS.Views
{
	public partial class FirstView : MvxViewController
	{
		private byte _clickedTimes;
		public FirstView () : base ("FirstView", null)
		{
			_clickedTimes = 0;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			var set = this.CreateBindingSet<FirstView, Core.ViewModels.FirstViewModel> ();
			set.Bind (Label).To (vm => vm.Hello);
			set.Bind (TextField).To (vm => vm.Hello);
			set.Apply ();

			var button = UIButton.FromType (UIButtonType.Custom);
			button.Frame = new CoreGraphics.CGRect (0, 200, 320, 20);
			button.SetTitle ("Click me please", UIControlState.Normal);
			button.BackgroundColor = UIColor.Cyan;
			button.TouchUpInside += ButtonClicked;

			View.AddSubview (button);
		}

		public void ButtonClicked (object sender, EventArgs args)
		{
			_clickedTimes++;
			Label.Text = _clickedTimes.ToString();
		}
	}
}
